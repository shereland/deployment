# Deployment

This microservice is used for updating images to Docker Swarm automatically (only approved services).

The Docker socket port (2375) is accessible locally and blocked from the world. The deployment service accepts requests via HTTPS just to know that a service was updated and then itself update the newly updated microservice.
