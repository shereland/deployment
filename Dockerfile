FROM python:3.6-alpine

ENV FLASK_APP=/code/app.py

COPY requirements.txt /
RUN pip install -r requirements.txt

WORKDIR /code
COPY src /code

CMD ["flask", "run", "--host=0.0.0.0"]
