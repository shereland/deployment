from flask import Flask
from mail import send_admin_email
import docker

app = Flask(__name__)


AVAILABLE_SERVICES = [
    'graphql',
    'mail',
    'web',
]


@app.route('/deploy/<service>', methods=['POST'])
def deploy(service):
    if service not in AVAILABLE_SERVICES:
        send_admin_email('Deployment service not available',
                         'Trying to access ' + service)
        return 'error'

    image = 'registry.gitlab.com/shereland/%s:stable' % (service)
    name = 'shereland_%s' % (service)
    client = docker.from_env()

    hash_current = client.images.get(image).attrs['Id']
    new_image = client.images.pull(image)

    if new_image.attrs['Id'] != hash_current:
        service = client.services.list(filters={'name': name})[0]
        service.update(image=image, force_update=True)

    return ''
