from app import app
from unittest.mock import patch
import docker
import requests_mock
import unittest


class DockerClientMock():

    def __init__(self):
        self.containers = self.DockerContainersMock()
        self.images = self.DockerImagesMock()
        self.services = self.DockerServicesMock()

    class DockerContainersMock():

        def list(self):
            return []

    class DockerImagesMock():

        pull_called = False

        def pull(self, image):
            self.pull_called = True
            self.pull_called_image = image
            service = DockerClientMock.DockerServiceMock()
            service.attrs = {
                "Id": "sha256:new"
            }
            return service

        def get(self, image):
            image = DockerClientMock.DockerImageMock()
            image.attrs = {
                "Id": "sha256:old"
            }
            return image

    class DockerImageMock():

        pass

    class DockerServicesMock():

        def list(self, **kwargs):
            service = DockerClientMock.DockerServiceMock()
            service.attrs = {
                "Id": "sha256:new"
            }
            self.services = [service]
            self.update_called_name = kwargs['filters']['name']
            return self.services

    class DockerServiceMock():

        update_called = False

        def update(self, **kwargs):
            self.update_called = True
            self.update_called_image = kwargs['image']


class AppTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    def test_send_mail_if_service_not_available(self):
        with requests_mock.mock() as m:
            m.post('http://mail:4444/email/send',
                   text='not important')
            self.app.post('/deploy/not-existent-service')

    def test_service_pull_image_and_update(self):
        mock = DockerClientMock()
        with patch.object(docker, 'from_env', return_value=mock):
            self.app.post('/deploy/web')

        image_name = 'registry.gitlab.com/shereland/web:stable'
        service_name = 'shereland_web'

        assert mock.images.pull_called
        assert image_name == mock.images.pull_called_image

        assert mock.services.services[0].update_called
        assert image_name == mock.services.services[0].update_called_image
        assert service_name == mock.services.update_called_name
