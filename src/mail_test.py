from mail import send_admin_email
import unittest
import requests_mock


adapter = requests_mock.Adapter()


class TestMail(unittest.TestCase):

    def test_post_request_when_called(self):
        with requests_mock.mock() as m:
            m.post('http://mail:4444/email/send',
                   text='not important')
            send_admin_email('some title', 'some message')
        assert m.called
