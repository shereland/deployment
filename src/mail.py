import os
import requests


ADMIN_EMAIL = os.environ.get("ADMIN_EMAIL")


def send_admin_email(title, message):
    email_data = {
        "templateName": "admin",
        "templateOptions": {
            "title": title,
            "message": message,
        },
        "emailOptions": {
            "to": ADMIN_EMAIL,
        }
    }
    requests.post('http://mail:4444/email/send', json=email_data)
    # TODO: handle error
